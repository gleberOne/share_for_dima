import SchemaField from './SchemaField';
import React from 'react';

export default Component => ({ name, ...props }) => <SchemaField name={name} component={Component} {...props}/>