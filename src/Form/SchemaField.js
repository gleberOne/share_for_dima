import React, { memo, useContext, useEffect } from 'react';
import FormField from "./FormField";
import { UIContext, useCurrentLevelContext } from './context';

export const SchemaField = (props) => {
  const {name} = props;
  const {rootErrors, ...childContext} = useCurrentLevelContext(name);
  const uiContext = useContext(UIContext);

  const isNotExists = !childContext.schema;

  useEffect(() => {
    if(!uiContext){
      return;
    }
    uiContext.reg(name);
  }, [childContext.schema]);

  useEffect(() => {
    if(!uiContext){
      return;
    }
    if(isNotExists){
      uiContext.unReg(name);
    }
  }, [childContext.schema]);

  if (!childContext.schema) {
    return null;
  }
  return (
    <FormField
      {...childContext}
      {...props}
    />
  )
};

export default memo(SchemaField);