import React, { createContext, memo, useCallback, useState } from 'react';
import {uniq} from "lodash";
import { UIContext } from './context';

export const UIField = ({ path, children }) => {

  const [registered, setRegistered] = useState(null);

  const reg = useCallback((path) => {
    setRegistered(uniq(registered? [...registered, path]: [path]));
  }, [path, registered]);

  const unReg = useCallback((path) => {
    const update = registered.filter(r => r !== path);
    setRegistered(update);
  }, [path, registered]);

  return (
    <UIContext.Provider value={{
      reg,
      unReg
    }}>
      {registered? (registered.length > 0? children: null): children}
    </UIContext.Provider>
  );
};

export default memo(UIField);