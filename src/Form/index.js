import React, { PureComponent } from 'react';
import { FormContext } from './context';
import set from "lodash/fp/set";

export class Form extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      formData: props.initialState
    };
  }

  onFieldChange = ({key, value, path}) => {
    const update = set(path, value, this.state.formData);
    this.setState({ formData: update });
    this.props.onChange({ key, value, path, formData: update });
  };


  render() {

    const errors = this.props.errors || {};

    const provideFormContext = {
      schema: this.props.schema,
      formData: this.state.formData,
      onFieldChange: this.onFieldChange,
      implementations: this.props.implementations,
      path: null,
      errors: this.props.errors || {},
      rootErrors: errors,
    };


    return (
      <FormContext.Provider value={provideFormContext}>
        <form>
          {this.props.render(provideFormContext)}
        </form>
      </FormContext.Provider>
    );
  }
}

export default Form;