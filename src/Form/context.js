import { createContext, useContext } from 'react';

export const FormContext = createContext(null);

export const UIContext = createContext(null);

export const useCurrentLevelContext = (name) => {
  const context = useContext(FormContext);
  const {formData, schema, path, ...props} = context;
  const newPath = path? `${path}.${name}`: name;

  return ({
    ...props,
    formData: formData[name],
    schema: schema && schema.properties && schema.properties[name],
    path: newPath,
    onFieldChange: props.onFieldChange,
    errors: props.rootErrors[newPath] || null,
  })
};