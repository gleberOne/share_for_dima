import React, { memo } from "react";

export const FormField = (props) => {
    const FF = props.formField || props.implementations.formField;
    const errors = props.errors || props.implementations.errors;
    return <FF {...props} errors={errors}/>;
};

export default memo(FormField);