import React from 'react';
import { useCurrentLevelContext, FormContext } from "./context";

export const Scope = (props) =>{
  const context = useCurrentLevelContext(props.name);
  return (
    <FormContext.Provider value={context}>
      {props.children}
    </FormContext.Provider>
  )
};

export default Scope;