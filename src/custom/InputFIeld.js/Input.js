import React, { useCallback } from 'react';
import asField from '../../Form/asField';

export const Input = ({ formData, name, onFieldChange, path, ...props }) => {
  const onChange = useCallback((e) => {
    onFieldChange({
      key: name,
      value: e.target.value,
      path,
    })
  }, [onFieldChange, name, path]);
  return (
    <input
      name={name}
      value={formData}
      onChange={onChange}
      {...props}
    />
  )
};

export default asField(Input);
