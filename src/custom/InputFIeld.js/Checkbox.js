import React, { useCallback } from 'react';
import asField from '../../Form/asField';

export const Checkbox = ({ formData, name, onFieldChange, path, ...props }) => {
  const onChange = useCallback((e) => {
    onFieldChange({
      key: name,
      value: !formData,
      path,
    });
  }, [onFieldChange, name, path, formData]);
  return (
    <input
      type="checkbox"
      name={name}
      checked={formData}
      onChange={onChange}
    />
  )
};

export default asField(Checkbox);
