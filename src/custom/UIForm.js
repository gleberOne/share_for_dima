import React, { createContext, PureComponent } from 'react';
import set from "lodash/fp/set";
import has from "lodash/fp/has";

export const UIContext = createContext(null);

export class UIForm extends PureComponent{

  constructor(props){
    super(props);
    this.state = { ui: { properties: {}} }
  }
  registerUIField = (path) => {
    console.log(path);
    const isExists = has(path, this.state.ui);
    console.log(this.state.ui);
    if(!isExists){
      this.setState({
        ui: set(path, {properties: {}}, this.state.ui)
      })
    }
  };

  render() {
    const provideUIContext = {
      registerUIField: this.registerUIField,
    };
    return (
      <UIContext.Provider value={provideUIContext}>
        {this.props.children}
      </UIContext.Provider>
    )
  }
}

export default UIForm;