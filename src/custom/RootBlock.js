import React, { useState, useCallback, useMemo } from 'react';
import { Container } from 'semantic-ui-react';
import validation from './validation';
import Engine from "json-rules-engine-simplified";
import { checkEvents } from 'fpn_react-json-schema-conditionals/dist';
import Form from '../Form';
import Scope from '../Form/Scope';
import InputField from './InputFIeld.js/Input';
import CheckboxField from './InputFIeld.js/Checkbox';
import { UIField } from '../Form/UIField';

const Schema = {
  properties: {
    field1: {
      type: 'string'
    },
    field2: {
      type: 'object',
      properties: {
        field3: {
          type: 'boolean'
        }
      }
    },
    field4: {
      type: 'object',
      properties: {
        field5: {
          type: 'string'
        }
      }
    },
    field6: {
      type: 'object',
      properties: {
        field7: {
          type: 'object',
          properties: {
            field8: {
              type: 'object',
              properties: {
                field9: {
                  type: 'string',
                }
              }
            }
          }
        },
      }
    },
    field10: {
      type: 'object',
      properties: {
        field11: {
          type: 'string'
        },
        field12: {
          type: 'string'
        }
      }
    }
  }
};


const Errors = ({ errors }) => {
  if(errors){
    return <div style={{color: "red", position: "absolute"}}>
      {
        Array.isArray(errors)? errors[0]: errors
      }
    </div>;
  }
  return null;
};

const FormField = ({ label, component, error, ...props }) => {
  const Component = component;
  return (
    <div style={{marginTop: "30px"}}>
      <p style={{fontSize: "20px", marginBottom: "5px"}}>{label}</p>
      <Component {...props}/>
      <Errors {...props}/>
    </div>
  )
};

const implementations = {
  formField: FormField
};

const rules = [];


export const RootBlock = ({ onChange }) => {
  const [errors, setErrors] = useState({});
  const [schema, setSchema] = useState(Schema);

  const initialState = {
    field1: "",
    field2: {
      field3: false
    },
    field4: {
      field5: ""
    },
    field6: {
      field7: {
        field8: {
          field9: ""
        }
      }
    },
    field10: {

    }
  };

  const engine = useMemo(() => {
    const eng =  new Engine([], Schema);
    rules.forEach(r => eng.addRule(r));
    return eng
  },[]);


  const onFieldChange = useCallback(({key, value, formData}) => {
    checkEvents(engine, formData, {schema: Schema, uiSchema: {}, actions: []})
      .then(({schema}) => {
        setSchema(schema);
        const newErrors = validation(formData);
        setErrors(newErrors);
      });
  }, [errors, setErrors]);

  return (
    <Container fluid>
      <Form
        onChange={onFieldChange}
        initialState={initialState}
        schema={schema}
        errors={errors}
        implementations={implementations}
        render={() => (
          <>
            <div style={{padding: "100px",}}>
              <h1>Форма</h1>
              <div style={{display: 'flex'}}>
                <div>
                  <InputField name="field1" label="Поле 😃"/>
                  <Scope name="field2">
                    <CheckboxField name="field3" label="Полечко 🥳🥳"/>
                  </Scope>
                  <Scope name="field4">
                    <InputField name="field5" label="Поле666"/>
                  </Scope>
                </div>
                <div style={{marginLeft: "50px"}}>
                  <Scope name="field6">
                    <Scope name="field7">
                      <Scope name="field8">
                        <InputField name="field9" label="Очень вложенный филд 🤙🤙🤙🤙🤙"/>
                      </Scope>
                    </Scope>
                  </Scope>
                </div>
                <div style={{marginLeft: "50px", border: "solid 1px"}}>
                  <UIField>
                    <Scope name="field10">
                      <InputField name="field11" label="Ввести сюда 100$ 🤑"/>
                      <InputField name="field12" label="Этот филд пропадет если... 🤡"/>
                    </Scope>
                  </UIField>
                </div>
              </div>
            </div>
          </>
        )}
      />
    </Container>
  );
};

export default RootBlock;