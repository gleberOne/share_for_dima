const validateField1 = (formData) => {
  if(formData.field1 && formData.field1 === ":D"){
    return {key: "field1", msg: "Не смейся"}
  }
};
const validateField2 = (formData) => {
  if(formData.field2 && formData.field2.field3 && formData.field2.field3 === true){
    return {key: "field2.field3", msg: "Не смей выделять меня"}
  }
};

const validations = [
  validateField1,
  validateField2
];

export default (formData, schema) => validations.reduce((acc, v) => {
  const result = v(formData);
  if(!result){
    return acc;
  }
  return {
    ...acc,
    [result.key]: acc[result.key]? [...acc[result.key], result.msg]: [result.msg]
  }
}, {})