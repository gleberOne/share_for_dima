/* eslint-disable */
import React, { useState, useMemo, memo } from 'react';
import './App.css';
import Form from "./custom/RootBlock";

const Child = memo((test) => {
  return "HELLLO!!!!!"
}, (_) => true)

const Parent = () => {
  const [test, setTest] = useState(null);

  return <div>
    <button onClick={() => setTest(Math.random())}>CLICK</button>
    <div style={{
      marginTop: "100px"
    }}>
      <Child test={test}/>
    </div>
  </div>
}

function App() {
  return (
    <Form/>
  );
}

export default App;
